<?php

////////////////////////// DB CONNECT ////////////////////////////////////

$host = 'localhost';
$user = 'WebDevTeam';
$pw = 'L34dG3n789';
$db = 'internal';

$conn = new mysqli($host, $user, $pw, $db);

if($conn->connect_errno){
	echo "Failed to connect to MySQL: " . $conn->connect_error;
  	exit();
}


////////////////////////// GET DATA ////////////////////////////////////

$q = "select employeeid, firstname, lastname from prlemployeemaster where active = 0";
$result = $conn->query($q);

$data = $result->fetch_all(MYSQLI_ASSOC);

$conn->close();

$json_data = json_encode($data);



////////////////////////// SEND REQUEST ////////////////////////////////////

// API url
$url = 'http://13.56.204.84/cron/data_migration/qa_audit/dst_qa_audit_employees.php';

// curl resource
$ch = curl_init($url);


// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

// Return response instead of outputting
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// session_write_close();

// Execute the POST request
$response = curl_exec($ch);

// Close cURL resource
curl_close($ch);


// http_response_code($response);

print_r($response);

