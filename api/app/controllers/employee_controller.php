<?php

class EmployeeController{
	public function index(){
		echo 'im in index';
	}

	public function get_user_details(){
		$conn = $this->dbconnect('tracker');

		$userid = isset($_GET['user']) ? $_GET['user'] : '';

		$q = "SELECT user, employeeid, smeid, tlid FROM userlist WHERE user = $userid";
		$result = $conn->query($q);

		$data = [];
		if($result->num_rows > 0){
			$data = $result->fetch_assoc();
		}

		$conn->close();

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function get_employee_details(){
		$conn = $this->dbconnect('internal');

		$employee_id = isset($_GET['employee_id']) ? $_GET['employee_id'] : '';

		$q = "SELECT pm.employeeid, concat(pm.firstname,' ',pm.lastname) as full_name, ts.teamsupervisor FROM prlemployeemaster pm LEFT JOIN teamassignment ta on ta.employeeid = pm.employeeid LEFT JOIN team_supervisor ts on ts.employeeid = ta.teamlead WHERE pm.employeeid = $employee_id";
		$result = $conn->query($q);

		$data = [];
		if($result->num_rows > 0){
			$data = $result->fetch_assoc();
		}

		$conn->close();
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function get_recording_location(){
		$recording_id = $_GET['recording_id'];
		$curl_conn = curl_init("http://38.107.183.5/api/get_recording_location.php?recording_id=$recording_id");
		curl_setopt($curl_conn, CURLOPT_RETURNTRANSFER, true);

		$json = '';
		if(($json = curl_exec($curl_conn)) === false){
			$empty = '';
			$json = json_encode($empty);
		}

		return $json;
	}

	public function update_hiredate(){
		$employeeid = isset($_GET['employeeid']) ? $_GET['employeeid'] : '';
		$hiredate = isset($_GET['hiredate']) ? $_GET['hiredate'] : '';

		$conn = $this->dbconnect('internal');

		$q = "UPDATE prlemployeemaster SET hiredate = '$hiredate' WHERE employeeid = '$employeeid'";
		$conn->query($q);
		if($conn->affected_rows > 0){
			$response = ['success' => 1];
		}else{
			$response = ['success' => 0];
		}

		$conn->close();

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	private function dbconnect($dbname){
		$host = 'localhost';
		$user = 'root';
		$pw = 'Snowfl@ke1';

		$conn = new mysqli($host, $user, $pw, $dbname);

		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}

		return $conn;
	}
}