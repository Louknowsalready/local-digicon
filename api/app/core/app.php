<?php

class App{

	public function __construct(){
		$controller = isset($_GET['controller']) ? $_GET['controller'] : '';

		try{
			$this->is_controller_exists($controller);
		}catch(Exception $e){
			header('Content-Type: application/json');
			echo 'Error: ' . $e->getMessage();
		}
		
	}

	private function is_controller_exists($controller){
		if(file_exists('app/controllers/'. $controller .'_controller.php')){
			require_once('app/controllers/'. $controller .'_controller.php');
			$className = $controller . 'controller';
			$controller_obj = new $className;

			$this->is_method_exists($controller_obj);
		}else{
			throw new Exception("Controller not found");
		}
	}

	private function is_method_exists($controller_obj){
		$method = isset($_GET['method']) ? $_GET['method'] : 'index';

		if(method_exists($controller_obj, $method)){
			call_user_func_array([$controller_obj, $method],[]);
		}else{
			throw new Exception("Method does not exist");
		}
	}
}