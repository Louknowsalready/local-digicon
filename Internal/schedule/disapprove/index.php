<?php require('employees.php'); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Disapprove Schedule</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/styles.css">


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" defer></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
	<script src="js/script.js" defer></script>
</head>
<body>
	<div class="container">
		<div class="row mb-4">
			<div class="col-md-12" id="message">
				
			</div>
		</div>

		<div class="row mb-5">
			<div class="col-md-12">
				<form action="disapprove.php" method="post">
					<div class="d-flex">
						<div class="form-inline mr-4">
							<label class="mr-2">Month</label>
							<select class="form-control form-control-sm" name="month">
								<option value="<?php echo $curr_mo ?>"><?php echo $curr_month ?></option>
							</select>
						</div>
						<div class="form-inline mr-4">
							<label class="mr-2">Year</label>
							<select class="form-control form-control-sm" name="year">
								<option value="<?php echo $curr_year ?>"><?php echo $curr_year ?></option>
							</select>
						</div>
						<div class="form-inline mr-4">
							<label class="mr-2">Payperiod</label>
							<select class="form-control form-control-sm" name="payperiod">
								<option value="10">10</option>
								<option value="25">25</option>
							</select>
						</div>
						<input type="submit" class="btn btn-sm btn-primary">
					</div>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="employee_area">
					<h4>Employee List</h4>
					<div class="input-group mb-3">
						<input type="text" class="form-control form-control-sm" placeholder="Search">
						<div class="input-group-append">
							<button class="btn btn-primary btn-sm" id="searchEmp" type="submit">Search</button>
						</div>
					</div>
					<div class="table-responsive box border">
						<table class="table">
							<thead>
								<tr>
									<th>Employeeid</th>
									<th colspan="2">Name</th>
								</tr>
							</thead>
							<tbody id='emp-list'>
								<?php
									foreach ($employees as $employee) {
								?>

										<tr>
											<td class="align-middle"><?php echo $employee['employeeid'] ?></td>
											<td class="align-middle"><?php echo implode(' ',[$employee['firstname'],$employee['lastname']]) ?></td>
											<td class="align-middle"><button class="btn btn-sm btn-success add">Add</button></td>
										</tr>

								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="employee_area">
					<h4>Added List</h4>
					<div class="input-group mb-3">
						<input type="text" class="form-control form-control-sm" placeholder="Search">
						<div class="input-group-append">
							<button class="btn btn-primary btn-sm" id="searchAdded" type="submit">Search</button>
						</div>
					</div>
					<div class="table-responsive box border">
						<table class="table">
							<thead>
								<tr>
									<th>Employeeid</th>
									<th colspan="2">Name</th>
								</tr>
							</thead>
							<tbody id="emp-added">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</body>
</html>